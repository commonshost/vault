### CORE calls VAULT to generate new certificate

Can happen on new domain deployment, or if user updates their SAN, or other manual trigger by user to regenerate (e.g. typically when setting up their domain).

1. Vault performs the LetsEncrypt challenge/response to obtain a new certificate.
1. Vault calls core to update the certificate (see below)
1. API call stays open as long as LetsEncrypt is pending?

-- Modify Domain+SAN

1. Core checks if fresh certificate with same domain+san already exists in database. If so, nothing to do.
1. Core stores requested domain+san in DB for future matching with issued cert. Overwrite previous request for that domain.
1. Core publishes message to request a certificate. Payload includes requested domain+san.
1. Vault receives message. Vault checks an ACME challenge with same domain+san is currently ongoing. If so, nothing to do.
1. Vault proceeds with ACME challenge. Vault emits failure or success message including domain+san and the certificate payload.
1. Core receives message. Checks if certificate for domain has matching request in DB. If non found or if san differs, ignore the message (outdated request).
1. Core stores certificate in DB.

-- Renew Domain+SAN

1. Core looks in database for stale certificates.
1. For each stale certificate, Core publishes message to request a certificate. Payload includes current domain+san.
1. Continue as per "Modify Domain+SAN" flow...

Message:

```js
{
  id: '564684846219841981234',
  subject: '',
  domain: 'example.com',
  san: ['www.example.com', 'example.net', 'api.example.net']
}
```

### VAULT calls CORE to issue certificate

Happens when LetsEncrypt succeeds.

1. Core stores key/cert/ca files on S3
1. Core updates MongoDB certificates collection
1. Core broadcasts `issue` on PubNub

Message:

```
{
  domain: 'example.com'
}
```

### VAULT calls CORE to revoke certificate

Happens when certificate expires? Fails to renew?

Poll at interval to query the MongoDB through CORE to get outdated certs, then automatically try to renew them.

1. Core stores key/cert/ca files on S3
1. Core updates MongoDB certificates collection
1. Core broadcasts `revoke` on PubNub

### VAULT refreshes certificates

Interval to invoke `acme.sh --refreshAll`

Callback script with a random secret key as environment variable or argument. Calls local URL with the domain name. Main VAULT process then uses acme.sh to locate the updated key/cert/ca files. Call the CORE to issue them.

### Webroot ACME response server

Run a static server for acme.sh to serve the `webroot` directory.
