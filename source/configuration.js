module.exports = {
  https: {
    port: process.env.COMMONSHOST_VAULT_PORT,
    key: process.env.COMMONSHOST_VAULT_KEY,
    cert: process.env.COMMONSHOST_VAULT_CERT,
    ca: process.env.COMMONSHOST_VAULT_CA.split(/[,\s]+/)
  },
  acme: {
    acmesh: process.env.COMMONSHOST_VAULT_ACME_ACMESH,
    home: process.env.COMMONSHOST_VAULT_ACME_HOME,
    webroot: process.env.COMMONSHOST_VAULT_ACME_WEBROOT
  },
  core: {
    origin: process.env.COMMONSHOST_VAULT_CORE_ORIGIN,
    clientId: process.env.COMMONSHOST_VAULT_CORE_APPLICATION_CLIENT_ID,
    clientSecret: process.env.COMMONSHOST_VAULT_CORE_APPLICATION_CLIENT_SECRET
  },
  jwt: {
    issuer: process.env.COMMONSHOST_VAULT_API_ISSUER,
    audience: process.env.COMMONSHOST_VAULT_API_AUDIENCE
  }
}
