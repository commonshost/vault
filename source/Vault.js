const amqp = require('amqplib')
const fastify = require('fastify')
const pino = require('pino')
const fastifyStatic = require('fastify-static')
const { join } = require('path')
// const { mkdir } = require('fs').promises

const ACME_PREFIX = '/.well-known/acme-challenge/'

class Vault {
  constructor (configuration) {
    this.configuration = configuration
    this.connection = undefined
    this.channel = undefined
    this.fastify = undefined
    this.logger = undefined
  }

  async start () {
    this.connection = await amqp.connect(this.configuration.amqp.uri)
    this.channel = await this.connection.createChannel()
    await this.channel.assertQueue('certificate.request')
    await this.channel.consume(
      'certificate.request',
      this.receive.bind(this)
    )
    this.logger = pino()
    this.fastify = fastify({ logger: this.logger })
    const root = join(this.configuration.acme.webroot, ACME_PREFIX)
    // await mkdir(root, { recursive: true })
    this.fastify.register(fastifyStatic, {
      root,
      prefix: ACME_PREFIX
    })
    await this.fastify.listen(
      this.configuration.acme.port,
      this.configuration.acme.host
    )
  }

  async stop () {
    await this.channel.close()
    await this.connection.close()
    await this.fastify.close()
  }

  async receive (message) {
    if (message === null) return
    if (message.properties.contentType !== 'application/json') return
    const content = JSON.parse(message.content.toString())
    this.logger.info(content)
    const success = { domain: 'example.net', san: ['www.example.net'] }
    await this.channel.assertQueue('certificate.issued')
    this.channel.sendToQueue(
      'certificate.issued',
      Buffer.from(JSON.stringify(success)),
      { contentType: 'application/json' }
    )
    this.channel.ack(message)
  }
}

module.exports.Vault = Vault
