#!/usr/bin/env node

const { join, resolve } = require('path')
const server = require('./server')

if (process.argv[2] === undefined) {
  console.error(
    'Specify the path to a configuration file.\n' +
    `See: ${join(__dirname, 'configuration.js')}`
  )
  process.exit(1)
}

const configuration = require(resolve(process.cwd(), process.argv[2]))
server(configuration)
  .then((fastify) => {
    fastify.listen(configuration.https.port, '::', (error) => {
      if (error) {
        console.error(error)
        process.exit(1)
      }
    })
  })
