const test = require('blue-tape')
const { Vault } = require('..')
const amqp = require('amqplib')
const { join } = require('path')
const fetch = require('node-fetch')

function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time))
}

const configuration = {
  amqp: {
    uri: 'amqp://localhost'
  },
  acme: {
    port: 11111,
    host: 'localhost',
    acmesh: join(__dirname, 'helpers/s3cmd.js'),
    home: join(__dirname, 'fixtures/home'),
    webroot: join(__dirname, 'fixtures/webroot')
  }
}

let vault

test('Start Vault', async (t) => {
  vault = new Vault(configuration)
  await vault.start()
  sleep(1000)
})

test('Web server for ACME is listening', async (t) => {
  const url = `http://${configuration.acme.host}:${configuration.acme.port}` +
    '/.well-known/acme-challenge/challenge.txt'
  const response = await fetch(url)
  t.is(response.status, 200)
  t.is(await response.text(), 'Mock challenge')
})

test('Renew certificate', async (t) => {
  const connection = await amqp.connect('amqp://localhost')
  const channel = await connection.createChannel()
  const message = { domain: 'example.net', san: ['www.example.net'] }
  channel.sendToQueue(
    'certificate.request',
    Buffer.from(JSON.stringify(message)),
    { contentType: 'application/json' }
  )
  sleep(1000)
  await channel.close()
  await connection.close()
})

test('Stop Vault', async (t) => {
  await vault.stop()
})
